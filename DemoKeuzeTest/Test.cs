﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PersonaVragenCL;
using KeuzeTestElements;

namespace DemoKeuzeTest
{
    class Test
    {
        private PersonaVraagController personaController;
        //private KeuzeTestController keuzeTestController;
        private Toestand toestand;
        private SituatieRegister situatieRegister;
        List<string> keywordList;

        public Test()
        {
            initializePersonaItems();
            initializeSituaties();
            keywordList = personaController.BeginPersonaVragenTest();
            toestand = new Toestand(keywordList);
           Situatie situatie = situatieRegister.getVolgendeSituatie(toestand.getkeywords().ToArray());
            Console.WriteLine(situatie.ToString());
            Console.ReadLine();
        }

        private void initializePersonaItems()
        {
            PersonaVraag vraag1 = new PersonaVraag("Ben je man of vrouw?", new PersonaEigenschap("man", new []{"man"}), new PersonaEigenschap("vrouw", new []{"vrouw"}));
            PersonaVraag vraag2 = new PersonaVraag("Heb je kinderen?", new PersonaEigenschap("heeft kinderen", new []{ "kinderen" }), new PersonaEigenschap("geen kideren", new []{"geen kinderen"}));
            PersonaVraag vraag3 = new PersonaVraag("Je leeftijd is tussen", new PersonaEigenschap("[0-30]", new []{"jong"}), new PersonaEigenschap("[31-...", new []{"oud"}));
            PersonaVraag vraag4 = new PersonaVraag("Je nationaliteit is", new PersonaEigenschap("Belgisch", new []{"belgisch"}), new PersonaEigenschap("andere nationaliteit", new []{"buitenland"}));
            personaController = new PersonaVraagController();
            personaController.VoegPersonaVraagToe(vraag1);
            personaController.VoegPersonaVraagToe(vraag2);
            personaController.VoegPersonaVraagToe(vraag3);
            personaController.VoegPersonaVraagToe(vraag4);
        }

        private void initializeSituaties()
        {
            #region situatie1
            Optie _optie1 = new Optie("Je gaat op zoek naar een creche", new[] { "kinderen_in_creche" });
            Eigenschap eigenschap1 = new Eigenschap(Eigenschap.Type.Slecht, "Alles zit al vol");
            Eigenschap eigenschap2 = new Eigenschap(Eigenschap.Type.Slecht, "Je mist je kinderen");
            Eigenschap eigenschap3 = new Eigenschap(Eigenschap.Type.Slecht, "Je moet een deel van je budget aan besteden");
            _optie1.VoegEigenschapToe(eigenschap1);
            _optie1.VoegEigenschapToe(eigenschap2);
            _optie1.VoegEigenschapToe(eigenschap3);

            Optie _optie2 = new Optie("Je regelt de opvang zelf", new[] { "kinderen_thuis" });
            Eigenschap eigenschap4 = new Eigenschap(Eigenschap.Type.Goed, "Je ziet je kinderen wel");
            Eigenschap eigenschap5 = new Eigenschap(Eigenschap.Type.Slecht, "Er zijn weinig momenten die passen voor sollicitatie gespreken");
            Eigenschap eigenschap6 = new Eigenschap(Eigenschap.Type.Goed, "Je hebt geen kosten");
            _optie2.VoegEigenschapToe(eigenschap4);
            _optie2.VoegEigenschapToe(eigenschap5);
            _optie2.VoegEigenschapToe(eigenschap6);

            Vraag vraag = new Vraag("Je hebt 2 kinderen. Als je op zoek wil gaan naar werk, moet je hen naar de creche brengen, anders heb je geen ruimte om sollicitatie gesprekken te doen.");
            Situatie situatie1 = new Situatie(vraag, _optie1, _optie2, 0.5, new []{"kinderen"});

            #endregion
            Optie optie1 = new Optie("In antwerpen voor jouw budget", new []{"stad"});
            optie1.VoegEigenschapToe(new Eigenschap(Eigenschap.Type.Slecht, "Een kleine studio"));
            optie1.VoegEigenschapToe(new Eigenschap(Eigenschap.Type.Goed, "Onderkomen"));
            optie1.VoegEigenschapToe(new Eigenschap(Eigenschap.Type.Goed, "Meer jobkansen en veel activiteiten in de onmiddelijk omgeving"));
            Optie optie2 = new Optie("In een kleinere gemeente", new []{"buiten_stad"});
            optie2.VoegEigenschapToe(new Eigenschap(Eigenschap.Type.Goed, "Een groter apparetement"));
            optie2.VoegEigenschapToe(new Eigenschap(Eigenschap.Type.Goed, "In betere staat"));
            optie2.VoegEigenschapToe(new Eigenschap(Eigenschap.Type.Slecht, "Niet veel activiteit of kansen voor jobs in de directe omgeving"));
            optie2.VoegEigenschapToe(new Eigenschap(Eigenschap.Type.Slecht, "Een uur met het openbaar vervoer naar dichtsbijzijnde stad."));
            vraag = new Vraag("Je moet een nieuwe woning zoeken. Voor joug budget kan je ofwel kiezen voor een studio in Antwerpen of een groeter appartement in een kleinere gemeente");
            Situatie situatie2 = new Situatie(vraag, optie1, optie2, 0.7, new[] {"man"});
            #region situati2

            

                #endregion




            situatieRegister = new SituatieRegister();
            situatieRegister.voegSituatieToe(situatie1);
            situatieRegister.voegSituatieToe(situatie2);
        }
    }
}
