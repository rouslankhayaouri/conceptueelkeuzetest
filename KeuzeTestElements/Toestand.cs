﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeuzeTestElements
{
    public class Toestand
    {
        private List<string> keywordsList;

        public Toestand(List<string> personaKeywordsList)
        {
            keywordsList = new List<string>(personaKeywordsList);
        }

        public void VoegKeywordsToe(List<string> keywordsList)
        {
            foreach (string s in keywordsList)
            {
                this.keywordsList.Add(s);
            }
        }

        public List<string> getkeywords()
        {
            return new List<string>(keywordsList);
        }
    }
}
