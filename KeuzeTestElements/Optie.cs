﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KeuzeTestElements
{
    public class Optie
    {
        private List<string> keyWordList;
        private String optieTekst;
        private static List<Eigenschap> _eigenschappen;

        private Func<Eigenschap.Type, List<Eigenschap>> eigenschapFilter = delegate(Eigenschap.Type t)
        {
            return _eigenschappen.Where(e => e.GeType().Equals(t)).ToList();
        };

        
            
    

        public Optie(string optieTekst, string[] keywords)
        {
            keyWordList = new List<string>(keywords);
            this.optieTekst = optieTekst;
            _eigenschappen = new List<Eigenschap>();
        }

        public void VoegEigenschapToe(Eigenschap eigenschap)
        {
            _eigenschappen.Add(eigenschap);
        }

        public override string ToString()
        {
            StringBuilder tekst = new StringBuilder("Optie: {");
            tekst.Append(optieTekst).Append("}\n").Append("Eigenschappen:\n");
            foreach (var eigenschap in _eigenschappen)
            {
                tekst.Append(eigenschap).Append("\n");
            }
            return tekst.ToString();
        }

        public List<Eigenschap> GetEigenschappen()
        {
            return new List<Eigenschap>(_eigenschappen);
        }

        public List<Eigenschap> GetGoedEigenscahppen()
        {
            return eigenschapFilter(Eigenschap.Type.Goed);
        }
        public List<Eigenschap> GetSlechteEigenscahppen()
        {
            return eigenschapFilter(Eigenschap.Type.Slecht);
        }

        public List<string> GetKeyWordsList()
        {
            return new List<string>(keyWordList);
        }
    }
}
