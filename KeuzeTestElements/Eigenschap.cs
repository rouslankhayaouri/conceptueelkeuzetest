﻿using System;

namespace KeuzeTestElements
{
    public class Eigenschap
    {
        public enum Type
        {
            Goed, Slecht
        }

        private Type type;
        private String eigenschapTekst;

        public Eigenschap(Type type, string eigenschapTekst)
        {
            this.type = type;
            this.eigenschapTekst = eigenschapTekst;
        }

        public Type GeType()
        {
            return type;
        }

        public override string ToString()
        {
            if (type.Equals(Type.Goed))
            {
                return "+ " + eigenschapTekst;
            }
            else
            {
                return "- " + eigenschapTekst;
            }
        }
    }
}
