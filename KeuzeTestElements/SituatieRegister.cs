﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeuzeTestElements
{
    public class SituatieRegister
    {
        private List<Situatie> situatieList;

        public SituatieRegister()
        {
            situatieList = new List<Situatie>();
        }

        public void voegSituatieToe(Situatie situatie)
        {
            situatieList.Add(situatie);
        }

        public Situatie getVolgendeSituatie(string[] toestandKeywords)
        {
            List<Situatie> tempSituatiesList = new List<Situatie>();
            foreach (Situatie situatie in situatieList)
            {
                foreach (string s in situatie.getSituatieKeywords())
                {
                    if (toestandKeywords.Contains(s))
                    {
                        tempSituatiesList.Add(situatie);
                        break;
                    }
                }
            }
            //met random een uitkiezen
            Random random = new Random();
            int index = random.Next(tempSituatiesList.Count);
            return tempSituatiesList.ElementAt(index);
        }
    }
}
