﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KeuzeTestElements
{
    public class Situatie
    {
        private List<string> SituatieKeywordList;
        private double _voorkomenKans;
        private Vraag vraag;
        private Optie _optie1;
        private Optie _optie2;

        public Situatie(Vraag vraag, Optie optie1, Optie optie2, double kans, string[] keywords)
        {
            SituatieKeywordList = new List<string>(keywords);
            this.vraag = vraag;
            _optie1 = optie1;
            _optie2 = optie2;
            _voorkomenKans = validatePercentage(kans);
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("Situatie:\n").Append(vraag.getVraag()).Append("\n");
            builder.Append("Optie 1: ").Append(_optie1).Append("\n");
            builder.Append("Optie 2: ").Append(_optie2).Append("\n");
            return builder.ToString();
        }

        public void voegKeyWordToe(string keyword)
        {
            SituatieKeywordList.Add(keyword);
        }

        public void voegKeywordsToe(string[] keywordArray )
        {
            foreach (string s in keywordArray)
            {
                SituatieKeywordList.Add(s);
            }
        }

        public void SetVoorkomenKans(double kans)
        {
            _voorkomenKans = validatePercentage(kans);
        }

        public double GetVoorkomenKans()
        {
            return _voorkomenKans;
        }

        public Optie GetOptie1()
        {
            return _optie1;
        }

        public Optie GetOptie2()
        {
            return _optie2;
        }

        public List<string> getSituatieKeywords()
        {
            return new List<string>(SituatieKeywordList);
        }
        private double validatePercentage(double precentage)
        {
            if (precentage >= 1)
            {
                throw new ArgumentException("Precentage niet correct");
            }
            return precentage;
        }
    }
}
