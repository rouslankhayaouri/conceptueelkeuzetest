﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeuzeTestElements
{
    public class Vraag
    {
        private string vraag;

        public Vraag(string vraag)
        {
            this.vraag = vraag;
        }

        public string getVraag()
        {
            return vraag;
        }
    }
}
