﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeuzeTestElements
{
    public struct Gemeente
    {
        public int postcode
        {
            get; set;
        }

        public string gemeente
        {
            get; set;
        }
    }
}
