﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Versioning;
using System.Text;
using System.Threading.Tasks;

namespace KeuzeTestElements
{
    public class GemeenteRegister
    {
        private List<Gemeente> gemeentes;

        public GemeenteRegister()
        {
            gemeentes = new List<Gemeente>();
        }

        public Gemeente getGemente(int postcode)
        {
            return gemeentes.First(g => g.postcode.Equals(postcode));
        }

        public void importeerGemeentesFromCSV(string uri)
        {
            using (var fs = File.OpenRead(@uri))
            using (var reader = new StreamReader(fs))
            {
                Gemeente gemeente;
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');
                    gemeente = new Gemeente();
                    gemeente.postcode = Int32.Parse(values[0]);
                    gemeente.gemeente = values[1];
                    gemeentes.Add(gemeente);
                }
            }
        }

    }
}
