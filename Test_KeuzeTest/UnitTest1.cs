﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using KeuzeTestElements;

namespace Test_KeuzeTest
{
    [TestClass]
    public class UnitTest1
    {
        private Optie _optie1;
        private Optie _optie2;
        private Situatie _situatie;

        [TestMethod]
        public void TestVoorkomenKans()
        {
            Assert.AreEqual(_situatie.GetVoorkomenKans(), 0.5);
        }

        [TestMethod]
        public void TestEigenschapCount()
        {
           Assert.AreEqual(_optie1.GetEigenschappen().Count, 3);
        }

        [TestMethod]
        public void TestGoedeEigenschappenOptie2()
        {
            Assert.AreEqual(_situatie.GetOptie2().GetGoedEigenscahppen().Count, 2);
        }

        [TestMethod]
        public void TestSlechteEigenschappen()
        {
            Assert.AreEqual(_situatie.GetOptie2().GetSlechteEigenscahppen().Count, 1);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestOptieChancePrecentage()
        {
            _situatie.SetVoorkomenKans(4);
        }

        [TestInitialize]
        public void Initialize()
        {
            _optie1 = new Optie("Je gaat op zoek naar een creche", new []{"kinderen_in_creche"});
            Eigenschap eigenschap1 = new Eigenschap(Eigenschap.Type.Slecht, "Alles zit al vol");
            Eigenschap eigenschap2 = new Eigenschap(Eigenschap.Type.Slecht, "Je mist je kinderen");
            Eigenschap eigenschap3 = new Eigenschap(Eigenschap.Type.Slecht, "Je moet een deel van je budget aan besteden");
            _optie1.VoegEigenschapToe(eigenschap1);
            _optie1.VoegEigenschapToe(eigenschap2);
            _optie1.VoegEigenschapToe(eigenschap3);


            _optie2= new Optie("Je regelt de opvang zelf", new []{"kinderen_thuis"});
            Eigenschap eigenschap4 = new Eigenschap(Eigenschap.Type.Goed, "Je ziet je kinderen wel");
            Eigenschap eigenschap5 = new Eigenschap(Eigenschap.Type.Slecht, "Er zijn weinig momenten die passen voor sollicitatie gespreken");
            Eigenschap eigenschap6 = new Eigenschap(Eigenschap.Type.Goed, "Je hebt geen kosten");
            _optie2.VoegEigenschapToe(eigenschap4);
            _optie2.VoegEigenschapToe(eigenschap5);
            _optie2.VoegEigenschapToe(eigenschap6);

            Vraag vraag = new Vraag("Je hebt 2 kinderen. Als je op zoek wil gaan naar werk, moet je hen naar de creche brengen, anders heb je geen ruimte om sollicitatie gesprekken te doen.");

            _situatie = new Situatie(vraag, _optie1, _optie2, 0.5, new []{"vrouw"});

        }

     
    }
}
