﻿using System.Linq;
using PersonaVragenCL;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test_KeuzeTest
{
    [TestClass]
    public class PersonaVragenTest
    {
        private PersonaEigenschapRegister _register;
        private PersonaVraagController _controller;
        [TestMethod]
        public void TestPersonaVraagRegister()
        {
            PersonaEigenschap eigenschap1 = new PersonaEigenschap("man", new []{"man"});
            PersonaEigenschap eigenschap2 = new PersonaEigenschap("ongehuwd", new []{"ongehuwd"});
            _register.VoegEigenschapToe(eigenschap1);
            _register.VoegEigenschapToe(eigenschap2);
            Assert.IsTrue(_register.GetListPersonaEigenshappen().ElementAt(0).PersonaTekst.Equals("man"));
        }

        [TestInitialize]
        public void Initialize()
        {
            PersonaVraag vraag1 = new PersonaVraag("Ben je man of vrouw/", new PersonaEigenschap("man",new []{"man"}), new PersonaEigenschap("vrouw", new []{"vrouw"}) );
            PersonaVraag vraag2 = new PersonaVraag("Heb je kinderen?", new PersonaEigenschap("heeft kinderen", new []{"kinderen"}), new PersonaEigenschap("geen kideren", new []{"geen_kinderen"}));
            PersonaVraag vraag3 = new PersonaVraag("Je leeftijd is tussen", new PersonaEigenschap("[0-30]", new []{"jong"}), new PersonaEigenschap("[31-...", new []{"oud"}));
            PersonaVraag vraag4 = new PersonaVraag("Je nationaliteit is", new PersonaEigenschap("Belgisch", new []{"belg"}), new PersonaEigenschap("andere nationaliteit", new []{"buitenlander"}));
            _register = new PersonaEigenschapRegister();
            _controller = new PersonaVraagController();
            _controller.VoegPersonaVraagToe(vraag1);
            _controller.VoegPersonaVraagToe(vraag2);
            _controller.VoegPersonaVraagToe(vraag3);
            _controller.VoegPersonaVraagToe(vraag4);
            _register = new PersonaEigenschapRegister();
        }
    }
}
