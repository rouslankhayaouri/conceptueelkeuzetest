﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using KeuzeTestElements;

namespace Test_KeuzeTest
{
    [TestClass]
    public class GemeenteTest
    {
        private GemeenteRegister register;
        [TestMethod]
        public void TestImports()
        {
            //StringReader data = new StringReader(Test_KeuzeTest.Properties.Resources.gemeentes1);
            //register.importeerGemeentesFromCSV(@"C:\Desktop\gemeentes1.csv");
            register.importeerGemeentesFromCSV(@"\\Mac\Home\Desktop\gemeentes1.csv");
            Assert.IsNotNull(register.getGemente(8511));
        }

        [TestMethod]
        public void Testcontents()
        {
            register.importeerGemeentesFromCSV(@"\\Mac\Home\Desktop\gemeentes1.csv");
            Assert.IsTrue(register.getGemente(3200).gemeente.Equals("Aarschot"));
        }

        [TestInitialize]
        public void initializeRegister()
        {
            register = new GemeenteRegister();
        }
    }
}
