﻿using System.Collections.Generic;
using System.Text;

namespace PersonaVragenCL
{
    public class PersonaEigenschapRegister
    {
        private List<PersonaEigenschap> persoonageEigenschappen;

        public PersonaEigenschapRegister()
        {
            persoonageEigenschappen = new List<PersonaEigenschap>();
        }

        public void VoegEigenschapToe(PersonaEigenschap eigenschap)
        {
            persoonageEigenschappen.Add(eigenschap);
        }

        public override string ToString()
        {
            StringBuilder eigenschappen = new StringBuilder();
            foreach (PersonaEigenschap eigenschap in persoonageEigenschappen)
            {
                eigenschappen.Append("-\t").Append(eigenschap).Append("\n");
            }
            return eigenschappen.ToString();
        }

        public List<PersonaEigenschap> GetListPersonaEigenshappen()
        {
            return new List<PersonaEigenschap>(persoonageEigenschappen);
        }

        public List<string> GetKeywordList()
        {
            List<string> personaKeywords = new List<string>();
            foreach (PersonaEigenschap eigenschap in persoonageEigenschappen)
            {
                foreach (string s in eigenschap.GetKeyWordList())
                {
                    personaKeywords.Add(s);
                }
            }
            return personaKeywords;
        }
    }
}
