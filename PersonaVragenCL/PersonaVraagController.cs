﻿using System;
using System.Collections.Generic;

namespace PersonaVragenCL
{
    public class PersonaVraagController
    {
        private List<PersonaVraag> personaVrageLijst;
        private PersonaEigenschapRegister register;

        public PersonaVraagController()
        {
            personaVrageLijst = new List<PersonaVraag>();
            register = new PersonaEigenschapRegister();
        }

        public void VoegPersonaVraagToe(PersonaVraag personaVraag)
        {
            personaVrageLijst.Add(personaVraag);
        }

        private void StelVraag(PersonaVraag vraag)
        {
            Console.WriteLine(vraag.ToString());
            Console.WriteLine("Keuze 1 -> 1 \t keuze 2 -> 2");
            int optieInt = int.Parse(Console.ReadLine());
            PersonaEigenschap eigenschap;
            if (optieInt == 1)
            {
                eigenschap = vraag.MaakKeuze(PersonaVraag.Keuze.Optie1);
            }
            else
            {
                eigenschap = vraag.MaakKeuze(PersonaVraag.Keuze.Optie2);
            }
            register.VoegEigenschapToe(eigenschap);
        }

        public List<string> BeginPersonaVragenTest()
        {
            Console.WriteLine("Een paar vragen om uw persoonlijkheid te beschrijven");
            foreach (PersonaVraag vraag in personaVrageLijst)
            {
                StelVraag(vraag);
            }
            Console.WriteLine("Dank u");
            Console.WriteLine(register.ToString());
            Console.ReadLine();
            return getPersonaKeywords();
        }

        public List<string> getPersonaKeywords()
        {
            return register.GetKeywordList();
        }
    }
}
