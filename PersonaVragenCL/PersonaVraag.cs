﻿using System;
using System.Text;

namespace PersonaVragenCL
{
    public class PersonaVraag
    {
        private PersonaEigenschap keuze1;
        private PersonaEigenschap keuze2;
        public string PersonaVraagSteling { get; }
        public enum Keuze
        {
            Optie1, Optie2
        }

        public PersonaVraag(String personaVraagstelling, PersonaEigenschap keuze1, PersonaEigenschap keuze2)
        {
            this.keuze1 = keuze1;
            this.keuze2 = keuze2;
            PersonaVraagSteling = personaVraagstelling;
        }

        public PersonaEigenschap MaakKeuze(Keuze keuze)
        {
            if (keuze.Equals(Keuze.Optie1))
            {
                return keuze1;
            }
            else
            {
                return keuze2;
            }
        }

        public override string ToString()
        {
            StringBuilder vraagString = new StringBuilder();
            vraagString.Append(PersonaVraagSteling + "\n");
            vraagString.Append("Keuze1: ").Append(keuze1).Append("\t");
            vraagString.Append("Keuze2: ").Append(keuze2);
            return vraagString.ToString();
        }
    }
}
