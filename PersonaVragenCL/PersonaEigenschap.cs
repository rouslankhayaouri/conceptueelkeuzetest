﻿using System.Collections.Generic;

namespace PersonaVragenCL
{
    public class PersonaEigenschap
    {
        private List<string> keywordList;
        public string PersonaTekst { get; }

        public PersonaEigenschap(string personaTekst, string[] keyWordList )
        {
            keywordList = new List<string>(keyWordList);
            PersonaTekst = personaTekst;
        }

        public override string ToString()
        {
            return PersonaTekst;
        }

        public void voegKeywordToe(string keyword)
        {
            keywordList.Add(keyword);
        }

        public List<string> GetKeyWordList()
        {
            return new List<string>(keywordList);
        }
    }
}
